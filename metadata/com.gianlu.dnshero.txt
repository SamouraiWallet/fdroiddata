AntiFeatures:NonFreeNet
Categories:Internet
License:GPL-3.0-only
Web Site:
Source Code:https://github.com/devgianlu/DNSHero
Issue Tracker:https://github.com/devgianlu/DNSHero/issues
Donate:https://www.paypal.me/devgianlu

Auto Name:DNS Hero
Summary:Inspect DNS zones like a superhero
Description:
DNS Hero let you inspect DNS zones easily. This app will give you information
about the root nameserver, nameservers associated with the domain and various
other DNS records (A, AAAA, MX, CNAME, TXT, SOA). You can also view the domain
health as a diagnostic is run.

Built using the http://zone.vision API by DNSimple.
.

Repo Type:git
Repo:https://github.com/devgianlu/DNSHero

Build:1.0.5-foss,6
    commit=v1.0.5
    subdir=app
    gradle=foss
    srclibs=CommonUtils@a89ffaebd726b747d0cfcb4c67b66b5abfc5a9a2
    prebuild=sed -i -e '/maven.fabric.io/d' ../build.gradle && \
        sed -i -e 's#../CommonUtils#app/$$CommonUtils$$#' ../settings.gradle

Build:1.1.3,10
    commit=v1.1.3
    subdir=app
    submodules=yes
    gradle=foss
    prebuild=sed -i -e '/maven.fabric.io/d' ../build.gradle ../CommonUtils/build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.3
Current Version Code:10
