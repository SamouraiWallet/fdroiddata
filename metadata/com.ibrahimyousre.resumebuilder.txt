Categories:Writing
License:MIT
Web Site:
Source Code:https://github.com/IbrahimYousre/Resume-Builder
Issue Tracker:https://github.com/IbrahimYousre/Resume-Builder/issues

Auto Name:Resume Builder
Summary:An app to help you create your resume
Description:
This is a simple Android app that will help you build a beautiful resume.
.

Repo Type:git
Repo:https://github.com/IbrahimYousre/Resume-Builder.git

Build:1.1,2
    commit=v1.1.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1
Current Version Code:2
